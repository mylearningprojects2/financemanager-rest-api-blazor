﻿using Common.DataTransferObjects;

namespace Task_12_WebApi.Validators
{
    public class FinOperationDtoValidator
    {
        public bool CreateDtoIsValid(FinOperationCreateDto dto)
        {
            if (dto == null || dto.Amount <= 0)
                return false;

            return true;
        }

        public bool UpdateDtoIsValid(FinOperationUpdateDto dto)
        {
            if (dto == null || dto.Amount <= 0 || dto.Id <= 0)
                return false;

            return true;
        }
    }
}
