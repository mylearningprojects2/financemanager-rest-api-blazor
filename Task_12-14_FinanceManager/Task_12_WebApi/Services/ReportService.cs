﻿using Common.ViewModels;

namespace Task_12_WebApi.Services
{
    public class ReportService
    {
        private DbWorkerService _dbWorker;

        public ReportService(DbWorkerService dbWorker)
        {
            _dbWorker = dbWorker;
        }

        public async Task<PeriodReportVm> GetPeriodReportAsync(DateTime startDate, DateTime endDate)
        {
            try
            {
                //var temp = await GetListFinOperationsVmAsync(startDate, endDate);

                //var temp2 = temp.Select(operation =>
                //    new FinOperationListVm(
                //        Id: operation.Id,
                //        Date: operation.Date,
                //        Amount: operation.Amount,
                //        CategoryType: operation.CategoryType,
                //        Account: new AccountListVm(operation.AccountId, operation.AccountName),
                //        Category: new CategoryListVm(operation.CategoryId, operation.CategoryName)
                //        )).ToList();


                return new PeriodReportVm()
                {
                    StartDate = startDate,
                    EndDate = endDate,
                    TotalIncome = await _dbWorker.GetTotalIncomeAsync(startDate, endDate),
                    TotalExpenses = await _dbWorker.GetTotalExpenseAsync(startDate, endDate),
                    FinOperations = await GetListFinOperationsVmAsync(startDate, endDate)
                };
            }
            catch (Exception e)
            {
                throw new Exception($"GetPeriodReport error: {e.Message}", e.InnerException);
            }
        }
        //old logic
        //private async Task<List<FinOperationGetDto>> GetListFinOperationsVmAsync(DateTime startDate, DateTime endDate)
        //{
        //    var operationsFromDb = await _dbWorker.GetOperationsAsync(startDate, endDate);

        //    var operationsDto = operationsFromDb
        //        .Select(operation => new FinOperationGetDto
        //        {
        //            Id = operation.Id,
        //            AccountId = operation.AccountId,
        //            AccountName = operation.Account.Name,
        //            Amount = operation.Amount,
        //            CategoryId = operation.CategoryId,
        //            CategoryName = operation.Category.Name,
        //            Date = operation.Date,
        //            Description = operation.Description,
        //            CategoryType = operation.CategoryType
        //        }).ToList();

        //    return operationsDto;
        //}

        private async Task<List<FinOperationListVm>> GetListFinOperationsVmAsync(DateTime startDate, DateTime endDate)
        {
            var operationsFromDb = await _dbWorker.GetOperationsAsync(startDate, endDate);

            var operationsVm = operationsFromDb
                .Select(operation =>
                    new FinOperationListVm(
                        Id: operation.Id,
                        Date: operation.Date,
                        Amount: operation.Amount,
                        OperationType: operation.OperationType,
                        Account: new AccountListVm(operation.AccountId, operation.Account.Name),
                        Category: new CategoryListVm(operation.CategoryId, operation.Category.Name),
                        Description: operation.Description
                    )).ToList();

            return operationsVm;
        }
    }
}
