﻿using Common.DataTransferObjects;
using Common.Enums;
using Microsoft.EntityFrameworkCore;
using Task_12_WebApi.Models;

namespace Task_12_WebApi.Services
{
    public class CategoryService
    {
        private readonly AppDbContext _dbContext;
        public CategoryService(AppDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<int> CreateAsync(string name, OperationType operationType)
        {
            string trimmedName = name.Trim();

            bool unicName = !await _dbContext.Categories.AnyAsync(c => c.Name == trimmedName);

            if (!unicName)
                throw new Exception($"Category with Name = {name} and CategoryType = {operationType} already exists");

            var newCategory =
                await _dbContext.Categories.AddAsync(new Category() { Name = name, OperationType = operationType });

            await _dbContext.SaveChangesAsync();

            return newCategory.Entity.Id;
        }

        public async Task<bool> UpdateAsync(CategoryUpdateDto updatedCategory)
        {
            if (updatedCategory?.Name is null)
                return false;

            try
            {
                var category = await GetAsync(updatedCategory.Id);

                if (category is null)
                    return false;

                category.Name = updatedCategory.Name;

                _dbContext.Categories.Update(category);

                await _dbContext.SaveChangesAsync();

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception($"Category update error: {ex.Message}", ex);
            }
        }

        public async Task<bool> SoftDeleteAsync(int id)
        {
            try
            {
                var category = await GetAsync(id);

                await _dbContext.Entry(category)
                    .Collection(c => c.SubCategories)
                    .LoadAsync();

                if (HaveSubCategories(category))
                {
                    category = await SoftDeleteSubCategories(category);
                }

                category.IsDeleted = true;

                _dbContext.Categories.Update(category);

                await _dbContext.SaveChangesAsync();

                return true;
            }

            catch (Exception ex)
            {
                throw new Exception($"Delete category exception: {ex.Message}", ex);
            }
        }

        public async Task<bool> HardDeleteAsync(int id)
        {
            try
            {
                var category = await GetAsync(id);

                await _dbContext.Entry(category)
                    .Collection(c => c.FinOperations)
                    .LoadAsync();

                await _dbContext.Entry(category)
                    .Collection(c => c.SubCategories)
                    .LoadAsync();

                if (HaveSubCategories(category))
                    throw new Exception($"Can't delete category '{category.Name}' with ID={id} it has SubCategories");


                _dbContext.Categories.Remove(category);

                await _dbContext.SaveChangesAsync();

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception($"Delete category exception: {ex.Message}", ex);
            }
        }

        public async Task<List<Category>> GetAllByTypeAsync(OperationType type)
        {
            return await _dbContext.Categories.Where(cat => !cat.IsDeleted && cat.OperationType == type).ToListAsync();
        }

        public async Task<Category> GetAsync(int id)
        {
            var category = await _dbContext.Categories.FindAsync(id);

            if (category == null)
                throw new Exception($"Category with such Id={id} not founded");

            if (category.IsDeleted)
                throw new Exception($"Category with such Id={id} is deleted");

            return category;
        }

        private bool HaveSubCategories(Category category)
        {
            if (category.SubCategories == null || category.SubCategories.Count == 0)
                return false;

            return true;
        }

        private async Task<Category> SoftDeleteSubCategories(Category category)
        {
            if (category.SubCategories == null)
                throw new Exception($"SubCategory delete error: category with ID={category.Id} have no sub categories");

            var subCategories = category.SubCategories;

            foreach (var cat in subCategories)
            {
                await _dbContext.Entry(cat)
                    .Collection(c => c.SubCategories)
                    .LoadAsync();

                if (HaveSubCategories(cat))
                    SoftDeleteSubCategories(cat); // рекурсивно помічаємо вкладені підкатегорії даної підкатегорії, як видалені.

                cat.IsDeleted = true;
            }

            return category;
        }
    }
}
