﻿using Common.Enums;
using Microsoft.EntityFrameworkCore;
using Task_12_WebApi.Models;

namespace Task_12_WebApi.Services
{
    public class DbWorkerService
    {
        private AppDbContext _dbContext;
        public DbWorkerService(AppDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<double> GetTotalIncomeAsync(DateTime startDate, DateTime endDate)
        {
            double totalIncome =
                await _dbContext.FinOperations
                    .Where(op => op.Date.Date >= startDate.Date &&
                                            op.Date.Date <= endDate.Date &&
                                            op.OperationType == OperationType.Income)
                    .SumAsync(op => op.Amount);

            return totalIncome;
        }

        public async Task<double> GetTotalExpenseAsync(DateTime startDate, DateTime endDate)
        {
            double totalExpence =
                await _dbContext.FinOperations
                    .Where(op => op.Date.Date >= startDate.Date.Date &&
                                            op.Date.Date <= endDate.Date &&
                                            op.OperationType == OperationType.Expense)
                    .SumAsync(op => op.Amount);

            return totalExpence;
        }

        public async Task<IList<FinOperation>> GetOperationsAsync(DateTime startDate, DateTime endDate)
        {
            List<FinOperation> operations =
                await _dbContext.FinOperations
                    .Where(op => op.Date.Date >= startDate.Date.Date &&
                                            op.Date.Date <= endDate.Date.Date)
                    .Include(op => op.Account)
                    .Include(op => op.Category)
                    .ToListAsync();

            return operations;
        }
    }
}
