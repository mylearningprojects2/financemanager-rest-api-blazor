﻿using Common.DataTransferObjects;
using Microsoft.EntityFrameworkCore;
using Task_12_WebApi.Models;

namespace Task_12_WebApi.Services
{
    public class AccountService
    {
        private readonly AppDbContext _dbContext;
        public AccountService(AppDbContext dBContext)
        {
            _dbContext = dBContext;
        }

        public async Task<int> CreateAsync(string name, bool visibilityAtBalance = true)
        {
            string trimmedName = name.Trim();

            bool unicName = !await _dbContext.Accounts.AnyAsync(c => c.Name == trimmedName);

            if (!unicName)
            {
                throw new Exception($"Account with name {trimmedName} already exist");
            }

            var newAccount = await _dbContext.Accounts.AddAsync(
                new Account()
                {
                    Name = trimmedName,
                    VisibilityAtBalance = visibilityAtBalance
                });

            await _dbContext.SaveChangesAsync();

            return newAccount.Entity.Id;
        }

        public async Task<bool> UpdateAsync(AccountUpdateDto updatedAccount)
        {
            if (updatedAccount == null || updatedAccount.Id <= 0)
                return false;

            try
            {
                var account = await GetAsync(updatedAccount.Id);

                account.VisibilityAtBalance = updatedAccount.VisibilityAtBalance;
                account.Name = updatedAccount.Name;

                _dbContext.Accounts.Update(account);

                await _dbContext.SaveChangesAsync();

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception($"Account update error: {ex.Message}", ex);
            }
        }

        public async Task<bool> SoftDeleteAsync(int id)
        {
            if (id <= 0)
                return false;

            try
            {
                var account = await GetAsync(id);

                account.IsDeleted = true;

                _dbContext.Accounts.Update(account);

                await _dbContext.SaveChangesAsync();

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception($"Delete account error: {ex.Message}", ex);
            }
        }

        public async Task<bool> HardDeleteAsync(int id)
        {
            if (id <= 0)
                return false;

            try
            {
                var account = await GetAsync(id);

                await _dbContext.Entry(account)
                    .Collection(a => a.FinOperations)
                    .LoadAsync();

                if (HaveFinOperations(account))
                    throw new Exception($"Can't delete account '{account.Name}' with ID={id} it has FinOperations");

                _dbContext.Accounts.Remove(account);

                await _dbContext.SaveChangesAsync();

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception($"Delete account error: {ex.Message}", ex);
            }
        }

        public async Task<List<Account>> GetAllAsync()
        {
            List<Account> accountList = await _dbContext.Accounts.Where(o => !o.IsDeleted).ToListAsync();

            return accountList;
        }

        public async Task<Account> GetAsync(int id)
        {
            var account = await _dbContext.Accounts.FindAsync(id);

            if (account == null)
                throw new ArgumentNullException($"Account with such Id={id} not founded");

            if (account.IsDeleted)
                throw new Exception($"Account with such Id={id} is deleted");

            return account;
        }

        private bool HaveFinOperations(Account account)
        {
            return account.FinOperations?.Count > 0;
        }
    }
}
