﻿using Common.DataTransferObjects;
using Microsoft.EntityFrameworkCore;
using Task_12_WebApi.Models;

namespace Task_12_WebApi.Services
{
    public class FinOrerationService
    {
        private readonly AppDbContext _dbContext;
        private readonly AccountService _accountService;
        private readonly CategoryService _categoryService;

        public FinOrerationService(AppDbContext dbContext, AccountService accountService, CategoryService categoryService)
        {
            _dbContext = dbContext;
            _accountService = accountService;
            _categoryService = categoryService;
        }

        public async Task<int> CreateAsync(FinOperationCreateDto finOperationCreateDto)
        {
            if (finOperationCreateDto.Date == default)
                finOperationCreateDto.Date = DateTime.Now;

            try
            {
                Category category = await _categoryService.GetAsync(finOperationCreateDto.CategoryId);
                Account account = await _accountService.GetAsync(finOperationCreateDto.AccountId);

                var finOperation =
                    await _dbContext.AddAsync(
                        new FinOperation()
                        {
                            Date = finOperationCreateDto.Date,
                            Amount = finOperationCreateDto.Amount,
                            CategoryId = finOperationCreateDto.CategoryId,
                            Category = category,
                            AccountId = finOperationCreateDto.AccountId,
                            Account = account,
                            OperationType = finOperationCreateDto.OperationType,
                            Description = finOperationCreateDto.Description,
                        });

                await _dbContext.SaveChangesAsync();

                return finOperation.Entity.Id;
            }

            catch (Exception ex)
            {
                throw new Exception($"Operation create error: {ex.Message}", ex);
            }
        }

        public async Task<bool> UpdateAsync(FinOperationUpdateDto finOperationUpdateDto)
        {
            if (finOperationUpdateDto?.Id is null)
                return false;

            try
            {
                var finOperationFromDb = await GetAsync(finOperationUpdateDto.Id);

                if (finOperationFromDb is null)
                    return false;


                finOperationFromDb.Date = finOperationUpdateDto.Date;

                finOperationFromDb.Amount = finOperationUpdateDto.Amount;

                finOperationFromDb.Description = finOperationUpdateDto.Description;

                finOperationFromDb.AccountId = finOperationUpdateDto.AccountId;

                finOperationFromDb.CategoryId = finOperationUpdateDto.CategoryId;

                finOperationFromDb.OperationType = finOperationUpdateDto.OperationType;

                //var updatedFinOperation = CompareAndUpdate(finOperationFromDb, finOperationUpdateDto);

                //if (updatedFinOperation == null)
                //    return false;

                _dbContext.FinOperations.Update(finOperationFromDb);

                await _dbContext.SaveChangesAsync();

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception($"Operation update error: {ex.Message}", ex);
            }
        }

        public async Task<bool> DeleteAsync(int id)
        {
            try
            {
                var operation = await GetAsync(id);

                if (operation is null)
                    return false;

                _dbContext.FinOperations.Remove(operation);

                await _dbContext.SaveChangesAsync();

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception($"Delete operation error: {ex.Message}", ex);
            }
        }

        public async Task<FinOperation> GetAsync(int id)
        {
            var operation = await _dbContext.FinOperations
                .Include(o => o.Account)
                .Include(o => o.Category)
                .FirstOrDefaultAsync(o => o.Id == id);

            if (operation == null)
                throw new Exception($"FinOperation with such Id = {id} not founded");

            return operation;
        }

        public async Task<IEnumerable<FinOperation>> GetAllAsync()
        {
            var finOperationsList = await _dbContext.FinOperations.ToListAsync();

            return finOperationsList;
        }

        //private FinOperation CompareAndUpdate(FinOperation finOperationFromDb, FinOperationUpdateDto finOperationUpdateDto)
        //{
        //    bool result = false;

        //    if (finOperationUpdateDto.Date != null && finOperationUpdateDto.Date != finOperationFromDb.Date)
        //    {
        //        finOperationFromDb.Date = finOperationUpdateDto.Date;
        //        result = true;
        //    }

        //    if (finOperationUpdateDto.Amount != null && finOperationUpdateDto.Amount != finOperationFromDb.Amount)
        //    {
        //        finOperationFromDb.Amount = finOperationUpdateDto.Amount;
        //        result = true;
        //    }

        //    if (finOperationUpdateDto.AccountId != null &&
        //        finOperationUpdateDto.AccountId != finOperationFromDb.AccountId)
        //    {
        //        finOperationFromDb.AccountId = finOperationUpdateDto.AccountId;
        //        result = true;
        //    }

        //    if (finOperationUpdateDto.CategoryId != null &&
        //        finOperationUpdateDto.CategoryId != finOperationFromDb.CategoryId)
        //    {
        //        finOperationFromDb.CategoryId = finOperationUpdateDto.CategoryId;
        //        result = true;
        //    }

        //    if (finOperationUpdateDto.Description != null &&
        //        finOperationUpdateDto.Description != finOperationFromDb.Description)
        //    {
        //        finOperationFromDb.Description = finOperationUpdateDto.Description;
        //        result = true;
        //    }

        //    
        //    if (finOperationUpdateDto.CategoryType != null &&
        //        finOperationUpdateDto.CategoryType != finOperationFromDb.CategoryType)
        //    {
        //        //some more logic

        //        finOperationFromDb.CategoryType = finOperationUpdateDto.CategoryType;
        //        result = true;
        //    }

        //    if (result)
        //        return finOperationFromDb;

        //    else return null;

        //}
    }
}
