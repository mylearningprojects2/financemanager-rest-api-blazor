﻿namespace Task_12_WebApi.Models
{
    public class Account
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool VisibilityAtBalance { get; set; }
        public bool IsDeleted { get; set; }
        public List<FinOperation> FinOperations { get; set; }
    }
}
