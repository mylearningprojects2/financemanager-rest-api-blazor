﻿using Common.Enums;

namespace Task_12_WebApi.Models
{
    public class Category
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<Category>? SubCategories { get; set; }
        public OperationType OperationType { get; set; }
        public List<FinOperation> FinOperations { get; set; }
        public bool IsDeleted { get; set; }
    }
}
