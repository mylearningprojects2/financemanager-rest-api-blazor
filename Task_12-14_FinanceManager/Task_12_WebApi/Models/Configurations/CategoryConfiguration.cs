﻿using Common.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Task_12_WebApi.Models.Configurations
{
    public class CategoryConfiguration : IEntityTypeConfiguration<Category>
    {
        public void Configure(EntityTypeBuilder<Category> builder)
        {
            builder.ToTable("Categoryes");
            builder.HasKey(c => c.Id);
            builder.Property(c => c.Id).ValueGeneratedOnAdd();
            builder.Property(c => c.Name).IsRequired();
            builder.Property(c => c.OperationType).IsRequired();
            builder.Property(c => c.IsDeleted).HasDefaultValue(false);
            builder.Property(c => c.IsDeleted).IsRequired();
            builder.HasData(
                new Category
                {
                    Id = 1,
                    Name = "NoCategory",
                    OperationType = OperationType.Expense,
                },
                new Category
                {
                    Id = 2,
                    Name = "NoCategory",
                    OperationType = OperationType.Income,
                });
        }
    }
}
