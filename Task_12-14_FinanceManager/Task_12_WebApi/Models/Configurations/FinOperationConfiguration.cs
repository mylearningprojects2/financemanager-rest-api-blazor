﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Task_12_WebApi.Models.Configurations
{
    public class FinOperationConfiguration : IEntityTypeConfiguration<FinOperation>
    {
        public void Configure(EntityTypeBuilder<FinOperation> builder)
        {
            builder.ToTable("FinOperations");
            builder.HasKey(o => o.Id);
            builder.Property(o => o.Id).ValueGeneratedOnAdd();
            builder.Property(o => o.Date).IsRequired();
            builder.Property(o => o.Amount).IsRequired();
            builder.Property(o => o.OperationType).IsRequired();
            builder.HasOne(o => o.Category).WithMany(c => c.FinOperations).HasForeignKey(o => o.CategoryId);
            builder.HasOne(o => o.Account).WithMany(a => a.FinOperations).HasForeignKey(o => o.AccountId);
        }
    }
}
