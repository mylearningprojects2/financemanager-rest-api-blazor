﻿using Common.Enums;

namespace Task_12_WebApi.Models
{
    public class FinOperation
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public double Amount { get; set; }
        public OperationType OperationType { get; set; }
        public int CategoryId { get; set; }
        public Category Category { get; set; }
        public string? Description { get; set; }
        public int AccountId { get; set; }
        public Account Account { get; set; }

    }
}
