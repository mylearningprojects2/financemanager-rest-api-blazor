﻿using Common.DataTransferObjects;
using Common.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Task_12_WebApi.Services;
using Task_12_WebApi.Validators;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Task_12_WebApi.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class FinOperationController : ControllerBase
    {
        private readonly FinOrerationService _finOrerationService;
        private readonly FinOperationDtoValidator _validator;
        public FinOperationController(FinOrerationService finOrerationService)
        {
            _finOrerationService = finOrerationService;
            _validator = new();
        }

        // GET: api/<FinOperationController>/GetAll
        [HttpGet]
        public async Task<IEnumerable<FinOperationGetDto>> GetAllAsync()
        {
            var finOperations = await _finOrerationService.GetAllAsync();

            var result = finOperations
                .Select(operation => new FinOperationGetDto
                {
                    Id = operation.Id,
                    Date = DateTime.Now,
                    Amount = operation.Amount,
                    AccountId = operation.AccountId,
                    AccountName = operation.Account.Name,
                    CategoryId = operation.CategoryId,
                    CategoryName = operation.Category.Name,
                    Description = operation.Description,
                    OperationType = operation.OperationType
                }).ToList();

            return result;
        }

        // GET api/<FinOperationController>/Get
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync([FromRoute] int id)
        {
            if (id <= 0)
                return BadRequest("Invalid Id.");

            try
            {
                var finOperation = await _finOrerationService.GetAsync(id);
                //TODO +подрібно також підгрузити акк та категорію
                return Ok(new FinOperationElementVm()
                {
                    Id = finOperation.Id,
                    Date = finOperation.Date,
                    Amount = finOperation.Amount,
                    AccountId = finOperation.Account.Id,
                    AccountName = finOperation.Account.Name,
                    CategoryId = finOperation.Category.Id,
                    CategoryName = finOperation.Category.Name,
                    Description = finOperation.Description,
                    OperationType = finOperation.OperationType
                });
                //return Ok(new FinOperationGetDto
                //{
                //    Id = finOperation.Id,
                //    Date = finOperation.Date,
                //    Amount = finOperation.Amount,
                //    AccountId = finOperation.AccountId,
                //    CategoryId = finOperation.CategoryId,
                //    Description = finOperation.Description,
                //    OperationType = finOperation.OperationType
                //});
            }

            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // POST api/<FinOperationController>/PostAsync
        [HttpPost]
        public async Task<IActionResult> CreateAsync([FromBody] FinOperationCreateDto finOperationCreateDto)
        {
            if (!_validator.CreateDtoIsValid(finOperationCreateDto))
                return BadRequest("Invalid data.");

            try
            {
                var operationId = await _finOrerationService.CreateAsync(finOperationCreateDto);

                return Ok(operationId);
            }

            catch (Exception ex)
            {
                return BadRequest($"Failed to create with error: {ex.Message}");
            }
        }

        // PUT api/<FinOperationController>/Put
        [HttpPut]
        public async Task<IActionResult> UpdateAsync([FromBody] FinOperationUpdateDto finOperationUpdateDto)
        {
            if (!_validator.UpdateDtoIsValid(finOperationUpdateDto))
                return BadRequest("Invalid data.");

            try
            {
                var status = await _finOrerationService.UpdateAsync(finOperationUpdateDto);

                return Ok(status);
            }

            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // DELETE api/<FinOperationController>/Delete
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync([FromRoute] int id)
        {
            if (id <= 0)
                return BadRequest("Invalid Id)");

            try
            {
                var status = await _finOrerationService.DeleteAsync(id);

                return Ok(status);
            }

            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
