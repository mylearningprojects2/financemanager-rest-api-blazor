﻿using Common.DataTransferObjects;
using Common.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Task_12_WebApi.Services;

namespace Task_12_WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class AccountController : ControllerBase
    {
        private readonly AccountService _accountService;
        public AccountController(AccountService accountService)
        {
            _accountService = accountService;
        }
        //TODO прибрати сміття
        [HttpGet]
        public async Task<List<AccountListVm>> GetAllAsync()
        {
            var accounts = await _accountService.GetAllAsync();

            List<AccountListVm> result = accounts
                .Select(account => new AccountListVm(

                    Id: account.Id,
                    Name: account.Name
                //VisibilityAtBalance: account.VisibilityAtBalance
                )).ToList();

            return result;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync([FromRoute] int id)
        {
            if (id <= 0)
                return BadRequest("Invalid Id");

            try
            {
                var account = await _accountService.GetAsync(id);
                return Ok(new AccountGetDto()
                {
                    Id = account.Id,
                    Name = account.Name,
                    VisibilityAtBalance = account.VisibilityAtBalance
                });
            }

            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateAsync([FromBody] AccountCreateDto accountCreateDto)
        {
            try
            {
                return Ok(await _accountService.CreateAsync(accountCreateDto.Name, accountCreateDto.VisibilityAtBalance));
            }

            catch (Exception e)
            {
                return Conflict(e.Message);
            }
        }

        [HttpPut]
        public async Task<IActionResult> UpdateAsync([FromBody] AccountUpdateDto updatedAccount)
        {
            if (updatedAccount == null || updatedAccount.Id <= 0)
                return BadRequest("Invalid data.");

            try
            {
                return Ok(await _accountService.UpdateAsync(updatedAccount));
            }

            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> SoftDeleteAsync([FromRoute] int id)
        {
            if (id <= 0)
                return BadRequest("Invalid Id.");

            try
            {
                return Ok(await _accountService.SoftDeleteAsync(id));
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> HardDeleteAsync([FromRoute] int id)
        {
            if (id <= 0)
                return BadRequest("Invalid Id.");

            try
            {
                return Ok(await _accountService.HardDeleteAsync(id));
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }
    }
}
