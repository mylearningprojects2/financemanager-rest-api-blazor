﻿using Common.DataTransferObjects;
using Common.Enums;
using Common.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Task_12_WebApi.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Task_12_WebApi.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly CategoryService _categoryService;

        public CategoryController(CategoryService categoryService)
        {
            _categoryService = categoryService;
        }
        //TODO прибрати...
        //[HttpGet]
        //public async Task<IEnumerable<CategoryListVm>> GetAllByTypeAsync()
        //{
        //    var categoryes = await _categoryService.GetAllByTypeAsync(type);

        //    List<CategoryListVm> result = categoryes
        //        .Select(category => new CategoryListVm
        //        {
        //            Id = category.Id,
        //            Name = category.Name
        //        }).ToList();

        //    return result;
        //}

        // GET: api/<CategoryController>/GetAll
        [HttpGet("{type}")]
        public async Task<IEnumerable<CategoryListVm>> GetAllAsync([FromRoute] string type)
        {
            OperationType parsedType;
            bool parsed = OperationType.TryParse(type, out parsedType);
            var categoryes = await _categoryService.GetAllByTypeAsync(parsedType);

            List<CategoryListVm> result = categoryes
                .Select(category => new CategoryListVm
                {
                    Id = category.Id,
                    Name = category.Name
                }).ToList();

            return result;
        }

        // GET api/<CategoryController>/Get
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync([FromRoute] int id)
        {
            if (id <= 0)
                return BadRequest("Invalid ID");

            try
            {
                var category = await _categoryService.GetAsync(id);

                return Ok(new CategoryGetDto
                {
                    Id = category.Id,
                    Name = category.Name,
                    OperationType = category.OperationType
                });
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        // POST api/<CategoryController>/Create
        [HttpPost]
        public async Task<IActionResult> CreateAsync([FromBody] CategoryCreateDto category)
        {
            try
            {
                return Ok(await _categoryService.CreateAsync(category.Name, category.CategoryType));
            }

            catch (Exception ex)
            {
                return Conflict(BadRequest(ex.Message));
            }
        }

        // PUT api/<CategoryController>/Put
        [HttpPut]
        public async Task<IActionResult> UpdateAsync([FromBody] CategoryUpdateDto updatedCategory)
        {
            if (updatedCategory.Id <= 0 || updatedCategory.Name is null)
                return BadRequest("Invalid data");

            try
            {
                return Ok(await _categoryService.UpdateAsync(updatedCategory));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // DELETE api/<CategoryController>/SoftDelete
        [HttpDelete("{id}")]
        public async Task<IActionResult> SoftDeleteAsync([FromRoute] int id)
        {
            if (id <= 0)
                return BadRequest("Invalid Id.");

            try
            {
                return Ok(await _categoryService.SoftDeleteAsync(id));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> HardDeleteAsync([FromRoute] int id)
        {
            if (id <= 0)
                return BadRequest("Invalid Id.");

            try
            {
                return Ok(await _categoryService.HardDeleteAsync(id));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
