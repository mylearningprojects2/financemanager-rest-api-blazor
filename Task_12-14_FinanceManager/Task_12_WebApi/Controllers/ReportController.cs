﻿using Common.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Task_12_WebApi.Services;

namespace Task_12_WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class ReportController : ControllerBase
    {
        private readonly ReportService _reportService;

        public ReportController(ReportService reportService)
        {
            _reportService = reportService;
        }

        [HttpGet]
        public async Task<DailyReportVm> GetDailyReportAsync([FromQuery] DateTime date)
        {
            DateTime startDay = date.Date; //отримуємо початок дня
            DateTime endDay = date.AddDays(1).Date.AddSeconds(-1); //отримуємо кінець поточного дня

            var report = await _reportService.GetPeriodReportAsync(startDay, endDay);

            return new DailyReportVm
            {
                Date = date,
                TotalExpenses = report.TotalExpenses,
                TotalIncome = report.TotalIncome,
                FinOperations = report.FinOperations,
            };
        }

        [HttpGet]
        public async Task<PeriodReportVm> GetPeriodReport([FromQuery] DateTime startDate, [FromQuery] DateTime endDate)
        {
            return await _reportService.GetPeriodReportAsync(startDate, endDate);
        }
    }
}
