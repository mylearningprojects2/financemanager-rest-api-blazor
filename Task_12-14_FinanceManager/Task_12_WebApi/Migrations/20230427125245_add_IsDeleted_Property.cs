﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Task_12_WebApi.Migrations
{
    /// <inheritdoc />
    public partial class add_IsDeleted_Property : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "Categoryes",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "Accounts",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "Accounts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "IsDeleted" },
                values: new object[] { false });

            migrationBuilder.UpdateData(
                table: "Categoryes",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "IsDeleted" },
                values: new object[] { false });

            migrationBuilder.UpdateData(
                table: "Categoryes",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "IsDeleted" },
                values: new object[] { false });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "Categoryes");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "Accounts");
        }
    }
}
