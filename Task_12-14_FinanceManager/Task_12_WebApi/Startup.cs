﻿using Microsoft.EntityFrameworkCore;
using Task_12_WebApi.Models;
using Task_12_WebApi.Services;

namespace Task_12_WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddDbContext<AppDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddScoped<AccountService>();
            services.AddScoped<CategoryService>();
            services.AddScoped<FinOrerationService>();
            services.AddScoped<ReportService>();
            services.AddScoped<DbWorkerService>();

            //// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            services.AddEndpointsApiExplorer();

            services.AddSwaggerGen();
        }
    }
}
