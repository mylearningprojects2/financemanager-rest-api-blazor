﻿using Common.DataTransferObjects;
using Common.Enums;
using Common.ViewModels;

namespace Task_13_BlazorUI.Data
{
    public class CategoryService
    {
        private readonly AppHttpService _appHttpService;
        private readonly string _uri = "/api/Category";

        public CategoryService(AppHttpService appHttpService)
        {
            _appHttpService = appHttpService;
        }

        public async Task<int> CreateAsync(CategoryCreateDto createDto)
        {
            //TODO верифікацію виділити окремим класом / методом
            if (createDto == null)
                throw new Exception($"CategoryCreateData is null");

            string finalUri = string.Concat(_uri, "/Create");

            var result = await _appHttpService.PostAsync(finalUri, createDto);

            return result;
        }

        public async Task<bool> UpdateAsync(CategoryUpdateDto updateDto)
        {
            if (updateDto == null)
                throw new Exception($"CategoryUpdateData is null");

            string finalUri = string.Concat(_uri, "/Update");

            var result = await _appHttpService.PutAsync(finalUri, updateDto);

            return result;
        }

        public async Task<bool> SoftDeleteAsync(int id)
        {
            if (id <= 0)
                throw new Exception($"Wrong Id");

            string finalUri = string.Concat(_uri, $"/SoftDelete/{id}");

            var result = await _appHttpService.SoftDeleteAsync(finalUri);

            return result;
        }

        public async Task<CategoryElementVm> GetAsync(int id)
        {
            if (id <= 0)
                throw new Exception($"Wrong Id");

            string finalUri = string.Concat(_uri, $"/Get/{id}");

            var result = await _appHttpService.GetAsync<CategoryElementVm>(finalUri);

            return result;
        }

        public async Task<List<CategoryListVm>> GetAllByTypeAsync(OperationType type)
        {
            string finalUri = string.Concat(_uri, $"/GetAll/{type}");

            var result = await _appHttpService.GetAsync<List<CategoryListVm>>(finalUri);

            return result;
        }
    }
}
