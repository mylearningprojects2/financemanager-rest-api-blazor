﻿using Common.DataTransferObjects;
using Common.ViewModels;

namespace Task_13_BlazorUI.Data
{
    public class AccountService
    {
        private readonly AppHttpService _appHttpService;
        private string _uri = "/api/Account";
        public AccountService(AppHttpService appHttpService)
        {
            _appHttpService = appHttpService;
        }

        public async Task<int> CreateAsync(AccountCreateDto createDto)
        {
            if (createDto == null)
                throw new Exception($"AccountCreateData is null");

            string finalUri = string.Concat(_uri, "/Create");

            var result = await _appHttpService.PostAsync(finalUri, createDto);

            return result;
        }

        public async Task<bool> UpdateAsync(AccountUpdateDto updateDto)
        {
            if (updateDto == null)
                throw new Exception($"AccountUpdateData is null");

            string finalUri = string.Concat(_uri, "/Update");

            var result = await _appHttpService.PutAsync(finalUri, updateDto);

            return result;
        }

        public async Task<bool> SoftDeleteAsync(int id)
        {
            if (id <= 0)
                throw new Exception($"Wrong Id");

            string finalUri = string.Concat(_uri, $"/SoftDelete/{id}");

            var result = await _appHttpService.SoftDeleteAsync(finalUri);

            return result;
        }

        public async Task<AccountElementVm> GetAsync(int id)
        {
            if (id <= 0)
                throw new Exception($"Wrong Id");

            string finalUri = string.Concat(_uri, $"/Get/{id}");

            var result = await _appHttpService.GetAsync<AccountElementVm>(finalUri);

            return result;
        }

        public async Task<List<AccountListVm>> GetAllAsync()
        {
            string finalUri = string.Concat(_uri, $"/GetAll");

            var result = await _appHttpService.GetAsync<List<AccountListVm>>(finalUri);

            return result;
        }
    }
}
