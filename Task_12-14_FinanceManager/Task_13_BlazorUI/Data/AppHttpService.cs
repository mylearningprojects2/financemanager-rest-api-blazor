﻿using Newtonsoft.Json;
using System.Text;

namespace Task_13_BlazorUI.Data
{
    public class AppHttpService
    {
        private HttpClient _httpClient;
        public AppHttpService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }
        public async Task<int> PostAsync<T>(string uri, T obj) where T : class
        {
            //var finalUri = $"api{uri}";

            var json = JsonConvert.SerializeObject(obj);

            var content = new StringContent(json, Encoding.UTF8, "application/json");

            var responce = await _httpClient.PostAsync(uri, content);

            var responceContent = await responce.Content.ReadAsStringAsync();

            if (!responce.IsSuccessStatusCode)
                throw new Exception(responceContent);

            return JsonConvert.DeserializeObject<int>(responceContent);
        }

        public async Task<bool> PutAsync<T>(string uri, T obj) where T : class
        {
            //string finUri = $"/api{uri}";

            var json = JsonConvert.SerializeObject(obj);

            var content = new StringContent(json, Encoding.UTF8, "application/json");

            var responce = await _httpClient.PutAsync(uri, content);

            var responceContent = await responce.Content.ReadAsStringAsync();

            if (!responce.IsSuccessStatusCode)
                throw new Exception(responceContent);

            return JsonConvert.DeserializeObject<bool>(responceContent);
        }

        public async Task<bool> SoftDeleteAsync(string uri)
        {
            //string finUri = $"/api{uri}";

            var responce = await _httpClient.DeleteAsync(uri);

            var responceContent = await responce.Content.ReadAsStringAsync();

            if (!responce.IsSuccessStatusCode)
                throw new Exception(responceContent);

            return JsonConvert.DeserializeObject<bool>(responceContent);
        }

        public async Task<T> GetAsync<T>(string uri) where T : class
        {
            //string finUri = $"/api{uri}";

            var responce = await _httpClient.GetAsync(uri);

            var responceContent = await responce.Content.ReadAsStringAsync();

            if (!responce.IsSuccessStatusCode)
                throw new Exception(responceContent);

            return JsonConvert.DeserializeObject<T>(responceContent);
        }

    }
}
