﻿using Common.ViewModels;

namespace Task_13_BlazorUI.Data
{
    public class ReportService
    {
        private readonly HttpClient _httpClient;
        private readonly AppHttpService _appHttpService;
        public ReportService(AppHttpService appHttpService)
        {
            _appHttpService = appHttpService;
        }

        public async Task<DailyReportVm> GetDayReport(DateTime date)
        {
            string isoDate = date.ToString("yyyy-MM-dd");

            var response = await _appHttpService.GetAsync<DailyReportVm>($"/api/Report/GetDailyReport?date={isoDate}");

            if (response == null)
            {
                throw new Exception("Getted report is null =(");
            }

            return response;
        }

        public async Task<PeriodReportVm> GetPeriodReport(DateTime startDate, DateTime endDate)
        {
            string isoStartDate = startDate.ToString("yyyy-MM-dd");
            string isoEndDate = endDate.ToString("yyyy-MM-dd");

            var response =
                await _appHttpService.GetAsync<PeriodReportVm>(
                    $"/api/Report/GetPeriodReport?startDate={isoStartDate}&endDate={isoEndDate}");

            if (response == null)
            {
                throw new Exception("Getted report is null =(");
            }

            return response;
        }
    }
}
