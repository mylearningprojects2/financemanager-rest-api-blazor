﻿namespace Task_13_BlazorUI.Data
{
    public interface IEntityService
    {
        Task<int> CreateAsync(object createDto);
        Task<bool> UpdateAsync(object updateDto);
        Task<bool> SoftDeleteAsync(int id);
        Task<object> GetAsync(int id);
        Task<List<object>> GetAllAsync();
    }
}
