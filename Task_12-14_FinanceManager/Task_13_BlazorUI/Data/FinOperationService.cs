﻿using Common.DataTransferObjects;
using Common.Enums;
using Common.ViewModels;

namespace Task_13_BlazorUI.Data
{
    public class FinOperationService
    {
        private readonly AppHttpService _appHttpService;
        private readonly string _uri = "/api/Finoperation";

        public FinOperationService(AppHttpService appHttpService)
        {
            _appHttpService = appHttpService;
        }

        public async Task<int> CreateAsync(FinOperationCreateDto createDto)
        {
            if (createDto == null)
                throw new Exception($"FinOperationCreateData is null");

            string finalUri = string.Concat(_uri, "/Create");

            var result = await _appHttpService.PostAsync(finalUri, createDto);

            return result;
        }

        public async Task<bool> UpdateAsync(FinOperationUpdateDto updateDto)
        {
            if (updateDto == null)
                throw new Exception($"FinOperationUpdateData is null");

            string finalUri = string.Concat(_uri, "/Update");

            var result = await _appHttpService.PutAsync(finalUri, updateDto);

            return result;
        }

        public async Task<bool> SoftDeleteAsync(int id)
        {
            if (id <= 0)
                throw new Exception($"Wrong Id");

            string finalUri = string.Concat(_uri, $"/SoftDelete/{id}");

            var result = await _appHttpService.SoftDeleteAsync(finalUri);

            return result;
        }

        public async Task<FinOperationElementVm> GetAsync(int id)
        {
            if (id <= 0)
                throw new Exception($"Wrong Id");

            string finalUri = string.Concat(_uri, $"/Get/{id}");

            var result = await _appHttpService.GetAsync<FinOperationElementVm>(finalUri);

            return result;
        }

        public async Task<List<FinOperationListVm>> GetAllAsync()
        {
            string finalUri = string.Concat(_uri, $"/GetAll");

            var result = await _appHttpService.GetAsync<List<FinOperationListVm>>(finalUri);

            return result;
        }

        public async Task<List<FinOperationListVm>> GetAllByType(OperationType type)
        {
            string finalUri = string.Concat(_uri, $"/GetAll/{type}");

            var result = await _appHttpService.GetAsync<List<FinOperationListVm>>(finalUri);

            return result;
        }
    }
}
