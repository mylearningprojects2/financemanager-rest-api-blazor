﻿namespace Common.Enums
{
    public enum OperationType
    {
        Expense,
        Income,
        Transfer
    }
}
