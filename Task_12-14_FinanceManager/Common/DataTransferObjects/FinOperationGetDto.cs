﻿namespace Common.DataTransferObjects
{
    public class FinOperationGetDto : FinOperationDto
    {
        public int Id { get; set; }
        public string CategoryName { get; set; }
        public string AccountName { get; set; }

    }
}
