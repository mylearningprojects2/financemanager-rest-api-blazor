﻿namespace Common.DataTransferObjects
{
    public class FinOperationUpdateDto : FinOperationDto
    {
        public int Id { get; set; }
    }
}
