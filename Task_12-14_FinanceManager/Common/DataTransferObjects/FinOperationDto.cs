﻿using Common.Enums;

namespace Common.DataTransferObjects
{
    public class FinOperationDto
    {
        public DateTime Date { get; set; }
        public double Amount { get; set; }
        public OperationType OperationType { get; set; }
        public int CategoryId { get; set; }
        public string? Description { get; set; }
        public int AccountId { get; set; }
    }
}
