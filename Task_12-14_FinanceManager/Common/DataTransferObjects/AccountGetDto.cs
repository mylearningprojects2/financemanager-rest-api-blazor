﻿namespace Common.DataTransferObjects
{
    public class AccountGetDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool VisibilityAtBalance { get; set; }
    }
}
