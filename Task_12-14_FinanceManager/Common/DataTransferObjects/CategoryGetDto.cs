﻿using Common.Enums;

namespace Common.DataTransferObjects
{
    public class CategoryGetDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public OperationType OperationType { get; set; }
    }
}