﻿namespace Common.DataTransferObjects
{
    public class AccountCreateDto
    {
        public string Name { get; set; }
        public bool VisibilityAtBalance { get; set; }
    }
}
