﻿namespace Common.DataTransferObjects
{
    //public class CategoryUpdateDto
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //    public bool IsDeleted { get; set; }
    //    //public List<CategoryUpdateDto> SubCategories { get; set; }
    //}

    public record CategoryUpdateDto(
        int Id,
        string Name
    );
}
