﻿using Common.Enums;

namespace Common.DataTransferObjects
{
    public class CategoryCreateDto
    {
        public string Name { get; set; }
        public OperationType CategoryType { get; set; }
    }
}