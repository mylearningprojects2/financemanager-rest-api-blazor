﻿using Common.Enums;

namespace Common.ViewModels
{
    public class FinOperationElementVm
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public double Amount { get; set; }
        public OperationType OperationType { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string? Description { get; set; }
        public int AccountId { get; set; }
        public string AccountName { get; set; }
    }

    //public record FinOperationElementVm(
    //    int Id,
    //    DateTime Date,
    //    double Amount,
    //    OperationType OperationType,
    //    CategoryListVm Category,
    //    AccountElementVm Account,
    //    string? Description
    //    );

    public record FinOperationListVm(
        int Id,
        DateTime Date,
        double Amount,
        OperationType OperationType,
        AccountListVm Account,
        CategoryListVm Category,
        string? Description
        );
}
