﻿using Common.Enums;

namespace Common.ViewModels
{
    public class CategoryElementVm
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<CategoryListVm> SubCategories { get; set; }
        public OperationType CategoryType { get; set; }
    }
    //public record CategoryElementVm(
    //    int Id,
    //    string Name,
    //    List<CategoryListVm>? SubCategories,
    //    CategoryType CategoryType,
    //    List<FinOperationListVm> FinOperations
    //    );

    public class CategoryListVm
    {
        public CategoryListVm()
        {
        }

        public CategoryListVm(
            int id,
            string name)
        {
            Id = id;
            Name = name;
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public OperationType CategoryType { get; set; }
    }
}
