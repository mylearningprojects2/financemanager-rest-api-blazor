﻿namespace Common.ViewModels
{
    public class PeriodReportVm
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public double TotalIncome { get; set; }
        public double TotalExpenses { get; set; }
        public IList<FinOperationListVm> FinOperations { get; set; }

    }
}
