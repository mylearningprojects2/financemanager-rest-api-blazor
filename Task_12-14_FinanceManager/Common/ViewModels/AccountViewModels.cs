﻿namespace Common.ViewModels
{
    //public class AccountElementVm
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //    public bool VisibilityAtBalance { get; set; }
    //    public bool IsDeleted { get; set; }
    //}
    public class AccountElementVm
    {
        public AccountElementVm()
        { }
        public AccountElementVm(
            int id,
            string name,
            bool visibilityAtBalance
        )
        {
            Id = id;
            Name = name;
            VisibilityAtBalance = visibilityAtBalance;
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public bool VisibilityAtBalance { get; set; } = true;
    }
    //public record AccountElementVm(
    //    int Id,
    //    string Name,
    //    bool VisibilityAtBalance
    //);

    public record AccountListVm(
        int Id,
        string Name);
}
