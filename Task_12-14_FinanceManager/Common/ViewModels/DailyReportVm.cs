﻿namespace Common.ViewModels
{
    public class DailyReportVm
    {
        public DateTime Date { get; set; }
        public double TotalIncome { get; set; }
        public double TotalExpenses { get; set; }
        public IList<FinOperationListVm> FinOperations { get; set; }
    }
}
