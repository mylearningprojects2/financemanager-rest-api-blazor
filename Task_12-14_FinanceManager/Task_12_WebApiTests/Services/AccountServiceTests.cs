﻿using Common.Enums;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics;
using Task_12_WebApi.Models;

namespace Task_12_WebApi.Services.Tests
{
    [TestClass()]
    public class AccountServiceTests
    {
        private DbContextOptions<AppDbContext> _options;
        private AppDbContext _dbContext;
        private AccountService _accountService;

        [TestInitialize]
        public void TestInitialize()
        {
            // Налаштування DbContextOptions для тестової бази даних
            _options = new DbContextOptionsBuilder<AppDbContext>()
                .UseInMemoryDatabase(databaseName: "TestDatabase")
                .Options;

            // Створення нового інстанції DbContext для кожного тесту
            _dbContext = new AppDbContext(_options);
            _dbContext.Database.EnsureCreated();

            // Заповнення тестової бази даних даними
            _dbContext.Accounts.AddRange(new List<Account>
            {
                new Account { Name = "TestAccount1", VisibilityAtBalance = true, FinOperations = new List<FinOperation>(){new FinOperation(), new FinOperation()}},
                new Account { Name = "TestAccount2", VisibilityAtBalance = true },
                new Account { Name = "HidedTestAccount", VisibilityAtBalance = false }
            });

            _dbContext.Categories.AddRange(new List<Category>
            {
                new Category { Name = "Category1", OperationType = OperationType.Expense },
                new Category { Name = "Category2", OperationType = OperationType.Income },
                new Category { Name = "Category3", OperationType = OperationType.Expense }
            });

            _dbContext.FinOperations.AddRange(new List<FinOperation>
            {
                new FinOperation { Date = DateTime.Now, Amount = 10, OperationType = OperationType.Expense, AccountId = 1, CategoryId = 1 },
                new FinOperation { Date = DateTime.Now, Amount = 20, OperationType = OperationType.Income, AccountId = 2, CategoryId = 2 },
                new FinOperation { Date = DateTime.Now, Amount = 30, OperationType = OperationType.Expense, AccountId = 3, CategoryId = 3 }
            });

            _dbContext.SaveChanges();

            _accountService = new AccountService(_dbContext);
        }

        [TestCleanup]
        public void TestCleanup()
        {
            _dbContext.Database.EnsureDeleted();
        }


        [TestMethod()]
        [DataRow(5)]
        public async Task CreateAsyncTest_AddingOneMoreAccount_IdIsEqual(int expectedId)
        {
            //Arrange
            var accountService = new AccountService(_dbContext);

            //Act
            var actualId = await accountService.CreateAsync("Test");

            Debug.WriteLine($"ActualId={actualId}");

            //Assert
            Assert.AreEqual(expectedId, actualId);
        }

        //[TestMethod()]
        //[DataRow(3, true)]
        //public async Task UpdateAsyncTest_onlyNotNullPropUpdate_passed(int accountId, bool expected)
        //{
        //    //Arrange
        //    var accountService = new AccountService(_dbContext);
        //    AccountUpdateDto accountUpdateDto = new AccountUpdateDto() { Name = "changedName" };
        //    //var accountBeforeUpdate = await AccountService.GetAsync(accountId);

        //    //Act
        //    await accountService.UpdateAsync(accountId, accountUpdateDto);
        //    var accountAfterUpdate = await AccountService.GetAsync(accountId);

        //    //Assert
        //    Assert.AreEqual(expected, accountAfterUpdate.VisibilityAtBalance);
        //}

        //[TestMethod()]
        //[DataRow(3, "changedName")]
        //public async Task UpdateAsyncTest_NameUpdate_passed(int accountId, string expected)
        //{
        //    //Arrange
        //    var accountService = new AccountService(_dbContext);
        //    AccountUpdateDto accountUpdateDto = new AccountUpdateDto() { Name = expected };

        //    //Act
        //    await accountService.UpdateAsync(accountId, accountUpdateDto);
        //    var accountAfterUpdate = await AccountService.GetAsync(accountId);

        //    //Assert
        //    Assert.AreEqual(expected, accountAfterUpdate.Name);
        //}

        [TestMethod()]
        [DataRow(4, true)]
        [DataRow(2, true)]
        public async Task DeleteAsyncTest(int accountId, bool expected)
        {
            //Arrange
            var accountService = new AccountService(_dbContext);

            //Act
            var actual = await accountService.SoftDeleteAsync(accountId);

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DataRow(10)]
        public async Task GetAsyncTest_notExistedId_Exception(int id)
        {
            //Act
            await Assert.ThrowsExceptionAsync<Exception>(() => _accountService.GetAsync(10));
        }
    }
}