﻿using Common.Enums;
using Microsoft.EntityFrameworkCore;
using Task_12_WebApi.Models;

namespace Task_12_WebApi.Services.Tests
{
    [TestClass()]
    public class DbWorkerServiceTests
    {
        private DbContextOptions<AppDbContext> _options;
        private AppDbContext _dbContext;
        private DbWorkerService _dbWorker;
        [TestInitialize]
        public void TestInitialize()
        {
            // Налаштування DbContextOptions для тестової бази даних
            _options = new DbContextOptionsBuilder<AppDbContext>()
                .UseInMemoryDatabase(databaseName: "TestDatabase")
                .Options;

            // Створення нової інстанції DbContext для кожного тесту
            _dbContext = new AppDbContext(_options);

            _dbWorker = new DbWorkerService(_dbContext);

            _dbContext.Database.EnsureCreated();

            // Заповнення тестової бази даних даними
            _dbContext.Accounts.AddRange(new List<Account>
            {
                new Account { Name = "TestAccount1", VisibilityAtBalance = true },
                new Account { Name = "TestAccount2", VisibilityAtBalance = true },
                new Account { Name = "HidedTestAccount", VisibilityAtBalance = false }
            });

            _dbContext.Categories.AddRange(new List<Category>
            {
                new Category { Name = "Category1", OperationType = OperationType.Expense },
                new Category { Name = "Category2", OperationType = OperationType.Income },
                new Category { Name = "Category3", OperationType = OperationType.Expense },
                new Category
                {
                    Name = "Category3",
                    OperationType = OperationType.Expense,
                    SubCategories = new List<Category>()
                    {
                        new Category { Name = "SubCategory1", OperationType = OperationType.Expense },
                        new Category { Name = "SubCategory2", OperationType = OperationType.Expense },
                        new Category { Name = "SubCategory3", OperationType = OperationType.Expense },
                    }
                }
            });

            _dbContext.FinOperations.AddRange(new List<FinOperation>
            {
                new FinOperation { Date = DateTime.Parse("05-03-2023"), Amount = 10, OperationType = OperationType.Expense, AccountId = 1, CategoryId = 1 },
                new FinOperation { Date = DateTime.Parse("05-03-2023"), Amount = 20, OperationType = OperationType.Income, AccountId = 2, CategoryId = 2 },
                new FinOperation { Date = DateTime.Parse("05-03-2023"), Amount = 30, OperationType = OperationType.Expense, AccountId = 3, CategoryId = 3 },
                new FinOperation { Date = DateTime.Parse("06-03-2023"), Amount = 10, OperationType = OperationType.Expense, AccountId = 1, CategoryId = 1 },
                new FinOperation { Date = DateTime.Parse("06-03-2023"), Amount = 20, OperationType = OperationType.Income, AccountId = 2, CategoryId = 2 },
                new FinOperation { Date = DateTime.Parse("06-03-2023"), Amount = 40, OperationType = OperationType.Expense, AccountId = 3, CategoryId = 3 },
                new FinOperation { Date = DateTime.Parse("07-03-2023"), Amount = 5, OperationType = OperationType.Expense, AccountId = 1, CategoryId = 1 },
                new FinOperation { Date = DateTime.Parse("08-03-2023"), Amount = 20, OperationType = OperationType.Income, AccountId = 2, CategoryId = 2 },
                new FinOperation { Date = DateTime.Parse("07-03-2023"), Amount = 30, OperationType = OperationType.Expense, AccountId = 3, CategoryId = 3 }

            });

            _dbContext.SaveChanges();
        }

        [TestCleanup]
        public void TestCleanup()
        {
            _dbContext.Database.EnsureDeleted();
        }
        //[TestMethod()]
        //[DataRow(20)]
        //public void GetTotalIncomeAsyncTest()
        //{
        //    Assert.AreEqual(expected, actual);
        //}

        //[TestMethod()]
        //public void GetTotalIncomeAsyncTest1()
        //{
        //    Assert.Fail();
        //}

        [TestMethod()]
        [DataRow("05-03-2023", "05-03-2023", (double)40)]
        public async Task GetTotalExpenseAsyncTest(string startDate, string endDate, double expected)
        {
            var result = await _dbWorker.GetTotalExpenseAsync(DateTime.Parse(startDate), DateTime.Parse(endDate));

            Assert.AreEqual(expected, result);
        }

        //[TestMethod()]
        //public void GetTotalExpenseAsyncTest1()
        //{
        //    Assert.Fail();
        //}

        //[TestMethod()]
        //public void GetOperationsAsyncTest()
        //{
        //    Assert.Fail();
        //}

        //[TestMethod()]
        //public void GetOperationsAsyncTest1()
        //{
        //    Assert.Fail();
        //}
    }
}